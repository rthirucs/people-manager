package com.flightright.peoplemanager.controller;

import com.flightright.peoplemanager.exception.MemberNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ResponseStatus(value = NOT_FOUND)
    @ExceptionHandler(value = MemberNotFoundException.class)
    public ResponseEntity memberNotFoundException() {
        LOGGER.warn("Member does not exists in the database");
        return ResponseEntity.notFound().build();
    }
}