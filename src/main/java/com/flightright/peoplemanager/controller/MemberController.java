package com.flightright.peoplemanager.controller;

import com.flightright.peoplemanager.entity.Member;
import com.flightright.peoplemanager.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/members")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(method = POST)
    public ResponseEntity createMember(@Valid @RequestBody Member member) {
        Member createdMember = memberService.createMember(member);
        return ResponseEntity.created(URI.create("/members/" + createdMember.getId().toString())).build();
    }

    @RequestMapping(method = GET, value = "/{id}")
    public ResponseEntity getMember(@PathVariable UUID id) {
        Member member = memberService.getMember(id);
        return ResponseEntity.ok(member);
    }

    @RequestMapping(method = PUT)
    public ResponseEntity updateMember(@RequestBody Member member) {
        memberService.updateMember(member);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = PATCH, value = "/{id}")
    public ResponseEntity patchMember(@PathVariable UUID id, @RequestBody Map<String, String> map) {
        memberService.patchMember(id, map);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    public ResponseEntity deleteMember(@PathVariable UUID id) {
        memberService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(method = GET)
    public ResponseEntity findAll() {
        List<Member> all = memberService.findAll();
        return ResponseEntity.ok(all);
    }
}
