package com.flightright.peoplemanager.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Member {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @NotNull
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotNull
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "DATE_OF_BIRTH")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    public void withNewValues(Map map) {
        if (map.containsKey("firstName"))
            setFirstName((String) map.get("firstName"));
        if (map.containsKey("lastName"))
            setLastName((String) map.get("lastName"));
        if (map.containsKey("dateOfBirth"))
            setDateOfBirth(LocalDate.parse((String) map.get("dateOfBirth")));
        if (map.containsKey("postalCode"))
            setPostalCode((String) map.get("postalCode"));
    }
}
