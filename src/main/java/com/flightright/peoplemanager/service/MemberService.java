package com.flightright.peoplemanager.service;

import com.flightright.peoplemanager.entity.Member;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface MemberService {

    Member createMember(Member member);

    Member getMember(UUID id);

    void updateMember(Member member);

    void patchMember(UUID id, Map map);

    void delete(UUID id);

    List<Member> findAll();

}
