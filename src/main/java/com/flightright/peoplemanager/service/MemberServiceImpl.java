package com.flightright.peoplemanager.service;

import com.flightright.peoplemanager.entity.Member;
import com.flightright.peoplemanager.exception.MemberNotFoundException;
import com.flightright.peoplemanager.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository repository;

    @Override
    public Member createMember(Member member) {
        return repository.save(member);
    }

    @Override
    public Member getMember(UUID id) {
        Member member = repository.findOne(id);
        if (member == null)
            throw new MemberNotFoundException();
        return member;
    }

    @Override
    public void updateMember(Member member) {
        checkIfExists(member);
        repository.save(member);
    }

    @Override
    public void patchMember(UUID id, Map map) {
        checkIfExists(id);
        Member existingMember = repository.findOne(id);
        existingMember.withNewValues(map);
        repository.save(existingMember);
    }

    @Override
    public void delete(UUID id) {
        checkIfExists(id);
        repository.delete(id);
    }

    @Override
    public List<Member> findAll() {
        return stream(repository.findAll().spliterator(), false)
                .collect(toList());
    }

    private void checkIfExists(Member member) {
        checkIfExists(member.getId());
    }

    private void checkIfExists(UUID uuid) {
        if (!repository.exists(uuid))
            throw new MemberNotFoundException();
    }

}
