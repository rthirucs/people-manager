package com.flightright.peoplemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flightright.peoplemanager.repository.MemberRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.JsonFieldType.STRING;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedRequestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberControllerApiTest {

    private MockMvc mockMvc;

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target");

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MemberRepository repository;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).build();
    }

    @Test
    public void shouldCreateTheMember() throws Exception {
        Map<String, String> map = getMemberMap();

        mockMvc.perform(
                post("/members")
                        .contentType(APPLICATION_JSON)
                        .content(asJson(map)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("/members/")))
                .andDo(document("createMember",
                        requestFields(
                                fieldWithPath("firstName").description("First name of the member"),
                                fieldWithPath("lastName").description("Last name of the member"),
                                fieldWithPath("dateOfBirth").description("Date of Birth of the member"),
                                fieldWithPath("postalCode").description("Postal code (ZIP code) of the member")
                        )))
                .andReturn();
    }

    @Test
    public void shouldRetrieveAMemberBasedOnId() throws Exception {
        String url = createMember();

        mockMvc.perform(
                RestDocumentationRequestBuilders.get("/members/{id}", url.substring(9)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("firstName", is("Thiru")))
                .andExpect(jsonPath("lastName", is("Ravi")))
                .andExpect(jsonPath("dateOfBirth", is("1992-02-23")))
                .andExpect(jsonPath("postalCode", is("638052")))
                .andDo(document("getMember", pathParameters(
                        parameterWithName("id").description("Id of the member")
                )));
    }

    @Test
    public void shouldReturnNotFoundWhenMemberDoesNotExists() throws Exception {
        mockMvc.perform(
                get("/members/a9de011d-1cbe-46a0-924f-9bd253aaa305"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeleteAMemberBasedOnId() throws Exception {
        String url = createMember();

        mockMvc.perform(
                RestDocumentationRequestBuilders.delete("/members/{id}", url.substring(9)))
                .andExpect(status().isNoContent())
                .andDo(document("deleteMember", pathParameters(
                        parameterWithName("id").description("Id of the member")
                )));

        mockMvc.perform(
                get(url))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldUpdateTheMemberName() throws Exception {
        String url = createMember();
        Map<String, String> updatedMember = getMemberMap();
        updatedMember.put("firstName", "Donald");
        updatedMember.put("id", url.substring(9));

        mockMvc.perform(
                put("/members")
                        .contentType(APPLICATION_JSON)
                        .content(asJson(updatedMember)))
                .andExpect(status().isOk())
                .andDo(document("updateMember", requestFields(
                        fieldWithPath("id").description("Id of the member"),
                        fieldWithPath("firstName").description("First name of the member"),
                        fieldWithPath("lastName").description("Last name of the member"),
                        fieldWithPath("dateOfBirth").description("Date of Birth of the member"),
                        fieldWithPath("postalCode").description("Postal code (ZIP code) of the member")
                )));

        mockMvc.perform(
                get(url))
                .andExpect(jsonPath("firstName", is("Donald")));
    }

    @Test
    public void shouldPutTheMemberName() throws Exception {
        String url = createMember();
        Map<String, String> map = new HashMap<>();
        map.put("firstName", "Ravi");
        map.put("dateOfBirth", "1992-02-25");

        mockMvc.perform(
                patch("/members/{id}", url.substring(9))
                        .contentType(APPLICATION_JSON)
                        .content(asJson(map)))
                .andExpect(status().isOk())
                .andDo(document("patchMember", relaxedRequestFields(
                        fieldWithPath("firstName").type(STRING).optional().description("First name of the member"),
                        fieldWithPath("lastName").type(STRING).optional().description("Last name of the member"),
                        fieldWithPath("dateOfBirth").type(STRING).optional().description("Date of Birth of the member"),
                        fieldWithPath("postalCode").type(STRING).optional().description("Postal code (ZIP code) of the member")
                )));

        mockMvc.perform(
                get(url))
                .andExpect(jsonPath("firstName", is("Ravi")))
                .andExpect(jsonPath("dateOfBirth", is("1992-02-25")));
    }

    @Test
    public void shouldGetAllTheMembers() throws Exception {
        createMember();
        Map<String, String> newMember = getMemberMap();
        newMember.put("firstName", "Donald");
        mockMvc.perform(
                post("/members")
                        .contentType(APPLICATION_JSON)
                        .content(asJson(newMember)))
                .andExpect(status().isCreated());

        mockMvc.perform(
                get("/members"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].firstName", containsInAnyOrder("Donald", "Thiru")));
    }

    @After
    public void tearDown() throws Exception {
        repository.deleteAll();
    }

    private String createMember() throws Exception {
        Map<String, String> map = getMemberMap();
        MvcResult mvcResult = mockMvc.perform(
                post("/members")
                        .contentType(APPLICATION_JSON)
                        .content(asJson(map)))
                .andReturn();
        return mvcResult.getResponse().getRedirectedUrl();
    }

    private Map<String, String> getMemberMap() {
        Map<String, String> map = new HashMap<>();
        map.put("firstName", "Thiru");
        map.put("lastName", "Ravi");
        map.put("postalCode", "638052");
        map.put("dateOfBirth", "1992-02-23");
        return map;
    }

    private String asJson(Map map) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}