package com.flightright.peoplemanager.controller;

import com.flightright.peoplemanager.entity.Member;
import com.flightright.peoplemanager.service.MemberService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.time.LocalDate.MIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.HttpStatus.*;

public class MemberControllerTest {

    @InjectMocks
    private MemberController memberController;

    @Mock
    private MemberService service;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldCreateANewMember() throws Exception {
        Member member = Member.builder().id(UUID.randomUUID()).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(service.createMember(any())).thenReturn(member);

        ResponseEntity response = memberController.createMember(member);

        verify(service).createMember(member);
        assertThat(response.getStatusCode()).isEqualTo(CREATED);
    }

    @Test
    public void shouldReturnTheMemberBasedOnTheId() throws Exception {
        UUID id = UUID.randomUUID();
        Member member = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(service.getMember(any())).thenReturn(member);

        ResponseEntity response = memberController.getMember(id);

        verify(service).getMember(id);
        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody()).isEqualTo(member);
    }

    @Test
    public void shouldUpdateTheMemberBasedOnTheBody() throws Exception {
        UUID id = UUID.randomUUID();
        Member member = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();

        ResponseEntity responseEntity = memberController.updateMember(member);

        verify(service).updateMember(member);
        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldPatchTheMemberBasedOnTheBody() throws Exception {
        UUID id = UUID.randomUUID();
        Map<String, String> map = new HashMap<>();
        map.put("firstName", "Thiru");
        map.put("dateOfBirth", "1992-02-23");

        ResponseEntity responseEntity = memberController.patchMember(id, map);

        verify(service).patchMember(id, map);
        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldDeleteMemberBasedOnTheId() throws Exception {
        UUID id = UUID.randomUUID();

        ResponseEntity response = memberController.deleteMember(id);

        verify(service).delete(id);
        assertThat(response.getStatusCode()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void shouldGetAllTheMembers() throws Exception {
        UUID id = UUID.randomUUID();
        Member firstMember = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        UUID secondId = UUID.randomUUID();
        Member secondMember = Member.builder().id(secondId).firstName("Ravi").lastName("Thiru")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(service.findAll()).thenReturn(Arrays.asList(firstMember, secondMember));

        ResponseEntity responseEntity = memberController.findAll();

        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
        assertThat(responseEntity.getBody()).asList().containsExactlyInAnyOrder(firstMember, secondMember);
    }
}