package com.flightright.peoplemanager.service;

import com.flightright.peoplemanager.entity.Member;
import com.flightright.peoplemanager.exception.MemberNotFoundException;
import com.flightright.peoplemanager.repository.MemberRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.*;

import static java.time.LocalDate.MIN;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MemberServiceImplTest {

    @InjectMocks
    private MemberServiceImpl memberService;

    @Mock
    private MemberRepository repository;

    @Captor
    private ArgumentCaptor<Member> memberArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldSaveTheMemberToTheRepository() throws Exception {
        Member member = Member.builder().firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();

        memberService.createMember(member);

        verify(repository).save(member);
    }

    @Test
    public void shouldReturnTheMemberBasedOnTheId() throws Exception {
        UUID id = randomUUID();
        Member member = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(repository.findOne(any())).thenReturn(member);

        Member actualMember = memberService.getMember(id);

        verify(repository).findOne(id);
        assertThat(actualMember).isEqualTo(member);
    }

    @Test(expected = MemberNotFoundException.class)
    public void shouldThrowAnExceptionWhenTheResourceIsNotFound() throws Exception {
        UUID id = randomUUID();
        when(repository.findOne(any())).thenReturn(null);

        memberService.getMember(id);

        verify(repository).findOne(id);
    }

    @Test
    public void shouldCheckIfTheMemberAlreadyExistsWhileUpdating() throws Exception {
        UUID id = randomUUID();
        Member member = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(repository.exists(any())).thenReturn(true);

        memberService.updateMember(member);

        verify(repository).exists(id);
        verify(repository).save(member);
    }

    @Test(expected = MemberNotFoundException.class)
    public void shouldThrowExceptionWhenMemberNotExistsWhileUpdating() throws Exception {
        UUID id = randomUUID();
        Member member = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        when(repository.exists(any())).thenReturn(false);

        memberService.updateMember(member);

        verify(repository).exists(id);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldPatchTheDetailsWithTheMemberIfExists() throws Exception {
        UUID id = randomUUID();
        Member existingMember = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        Map<String, Object> map = new HashMap<>();
        map.put("postalCode", "638053");
        when(repository.exists(any())).thenReturn(true);
        when(repository.findOne(any())).thenReturn(existingMember);

        memberService.patchMember(id, map);

        verify(repository).exists(id);
        verify(repository).save(memberArgumentCaptor.capture());
        Member newMember = Member.builder().id(id).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638053").build();
        assertThat(memberArgumentCaptor.getValue()).isEqualTo(newMember);
    }

    @Test(expected = MemberNotFoundException.class)
    public void shouldThrowExceptionWhenMemberNotExistsWhilePatching() throws Exception {
        UUID id = randomUUID();
        Map<String, Object> map = new HashMap<>();
        map.put("postalCode", "638053");
        when(repository.exists(any())).thenReturn(false);

        memberService.patchMember(id, map);

        verify(repository).exists(id);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldDeleteTheMemberBasedOnTheId() throws Exception {
        UUID id = randomUUID();
        when(repository.exists(any())).thenReturn(true);

        memberService.delete(id);

        verify(repository).delete(id);
    }

    @Test(expected = MemberNotFoundException.class)
    public void shouldThrowExceptionWhenMemberNotExistsWhileDeleting() throws Exception {
        UUID id = randomUUID();
        when(repository.exists(any())).thenReturn(false);

        memberService.delete(id);

        verify(repository).exists(id);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnAllTheMemberFromTheRepository() throws Exception {
        Member firstMember = Member.builder().id(randomUUID()).firstName("Thiru").lastName("Ravi")
                .dateOfBirth(MIN).postalCode("638052").build();
        Member secondMember = Member.builder().id(randomUUID()).firstName("Ravi").lastName("Thiru")
                .dateOfBirth(MIN).postalCode("638053").build();
        when(repository.findAll()).thenReturn(Arrays.asList(firstMember, secondMember));

        List<Member> allMembers = memberService.findAll();

        assertThat(allMembers).containsExactlyInAnyOrder(firstMember, secondMember);
    }
}
