package com.flightright.peoplemanager.repository;

import com.flightright.peoplemanager.entity.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static java.time.LocalDate.MIN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class MemberRepositoryTest {

    @Autowired
    private MemberRepository repository;

    @Test
    public void shouldSaveTheMemberIntoTheDatabase() throws Exception {
        Member member = Member.builder().firstName("Thiru").lastName("Ravi").dateOfBirth(MIN).postalCode("638052").build();

        Member savedMember = repository.save(member);

        Iterable<Member> allMembers = repository.findAll();
        assertThat(allMembers).contains(savedMember);
    }

    @Test
    public void shouldReturnAllTheSavedMembers() throws Exception {
        Member savedFirstMember = repository.save(Member.builder().firstName("Thiru").lastName("Ravi").dateOfBirth(MIN).postalCode("638052").build());
        Member savedSecondMember = repository.save(Member.builder().firstName("Mike").lastName("Mac").dateOfBirth(MIN).postalCode("638052").build());

        Iterable<Member> allMembers = repository.findAll();

        assertThat(allMembers).containsExactlyInAnyOrder(savedFirstMember, savedSecondMember);
    }

    @Test
    public void shouldDeleteAMemberFromTheList() throws Exception {
        Member savedFirstMember = repository.save(Member.builder().firstName("Thiru").lastName("Ravi").dateOfBirth(MIN).postalCode("638052").build());
        repository.save(Member.builder().firstName("Mike").lastName("Mac").dateOfBirth(MIN).postalCode("638052").build());

        repository.delete(savedFirstMember.getId());

        Iterable<Member> allMembers = repository.findAll();
        assertThat(allMembers).doesNotContain(savedFirstMember);
    }
}