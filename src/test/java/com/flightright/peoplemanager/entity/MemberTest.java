package com.flightright.peoplemanager.entity;

import org.junit.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MemberTest {

    @Test
    public void shouldUpdateMemberWithValuesInMap() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("firstName", "Thiru");
        map.put("lastName", "Ravi");
        map.put("postalCode", "638052");
        map.put("dateOfBirth", "1992-02-23");
        Member member = Member.builder().firstName("T").lastName("R").postalCode("6").dateOfBirth(LocalDate.MIN).build();

        member.withNewValues(map);

        assertThat(member.getFirstName()).isEqualTo("Thiru");
        assertThat(member.getLastName()).isEqualTo("Ravi");
        assertThat(member.getPostalCode()).isEqualTo("638052");
        assertThat(member.getDateOfBirth().toString()).isEqualTo("1992-02-23");
    }
}