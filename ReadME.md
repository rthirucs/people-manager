# People Manager - Flightright #

The application (People manager) is meant for managing the people in the database. The application is developed using Spring Boot and uses HSQL in memory database.

### Prerequisites ###

* Java 8
* Maven
* Git
* [Lombok plugin](https://projectlombok.org/download) in corresponding IDE  

### How do I get set up? ###

Note: This application uses Lombok library. So, to view the source code without any errors, the lombok plugin has to be installed

**Installation** 
```
git clone git@bitbucket.org:rthirucs/people-manager.git
mvn clean install
mvn spring-boot:run
```

**To run the tests**
```
mvn clean test
```


### Pipeline ###

* Created a pipeline in BitBucket, which will run for every commit. [PipeLine](https://bitbucket.org/rthirucs/people-manager/addon/pipelines/home#!/)

### Tests ###

The service was written by following the TDD (Test Driven Development) approach. The code is completely test covered. 

### Documentation ###

API Documentation is written using Spring-Rest-Docs. Steps for generating the API Documentation.
```
mvn clean package
mvn spring-boot:run
```
The documentation can be accessed at [Link](http://localhost:8080/docs/index.html). Hosted doc [link](http://ec2-13-250-39-113.ap-southeast-1.compute.amazonaws.com:8080/docs/index.html)

### Hosting ###

The application is hosted in AWS [flightright people manager](http://ec2-13-250-39-113.ap-southeast-1.compute.amazonaws.com:8080/health)